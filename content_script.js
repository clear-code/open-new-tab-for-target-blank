/*
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/
'use strict';

window.addEventListener('click', event => {
  let link = event.target;
  if (!(link instanceof Element))
    link = link.parentNode;
  if (!link.href ||
      link.target != '_blank')
    return;

  browser.runtime.sendMessage({ type: 'link-clicked', href: link.href });
  event.stopImmediatePropagation();
  event.preventDefault();
  return false;
}, true);
