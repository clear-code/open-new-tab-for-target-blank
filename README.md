# open-new-tab-for-target-blank

This is for a speciific case: you want to open a new tab when a link with `target="_blank"` is clicked, on Firefox with following options:

```javascript
// Open all links in the current tab, despite the link has any "target" attribute.
user_pref("browser.link.open_newwindow", 1);
// Treat window.open() windows as the behavior defined with "browser.link.open_newwindow".
user_pref("browser.link.open_newwindow.restriction", 0);
// Open requests from other applications) in the current tab.
user_pref("browser.link.open_newwindow.override.external", 1);
```
